docker
=========

Install and configure Docker dependencies for SELinux, updated Docker sysctl


Tasks
----------

* docker_tools - installs Docker and python-pip
* docker       - installs docker python, adjuct Docker startup scripts and enable Docker

Requirements
------------

N/A

Role Variables (defaults/vars)
------------------------------

* docker-tools


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - docker

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
