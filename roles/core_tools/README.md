core_tools
=========

Install boto and botocore packages
Install if doesn't exist and configure AWS CLI


Tasks
----------

* boto    - ensures python-pip is present, installs boto, boto3 and botocore packages
* aws_cli - installs AWS CLI bundle if it doesn't exist and configure it


Requirements
------------

N/A

Role Variables (defaults/vars)
------------------------------

* boto_pip: - boto and botocore packages
** boto
** boto3
** botocore

* region - AWS configure set region
* region - AWS set region for the AWS CLI configuration



Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - core_tools

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
