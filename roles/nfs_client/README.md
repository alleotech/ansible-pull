nfs_client
=========
Install cachefilesd and mount the NFS

* install cachefilesd
* ensures NFS target dir is in place
* waits for NFS server
* enables cachefilesd
* mounts NFS on the path /mnt/data on a specified host

Tasks
-----

* nfs_client 

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

* nfs_server - the hostname of the nfs_server

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nfs_client

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
