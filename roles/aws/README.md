aws
=========

AWS tools and configuration, includes the following roles:

* aws_config  - set hostname, adjust resolve and cloud init
* nvme        - install and configure ebs mapping and udev rules

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - aws

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
