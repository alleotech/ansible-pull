Role base_tools
=========
Install base yum packages and tools:

Tasks
----------
* tools - installs base yum packages and tools declared in the defaults

Requirements
------------

N/A

Role Variables (Defaults)
--------------
* yum_packages:
** epel-release
** yum-utils

* base_tools:
** bind-utils
** git
** htop
** initscripts
** links
** mtr
** net-tools
** nfs-utils
** telnet
** tree
** unzip
** vim
** wget
** zip


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - base_tools

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
