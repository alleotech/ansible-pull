aws_config
=========

Configure the DNS with the following tasks:

* hostname     - set the proper hostname
* resolv       - adjust resolv config, replace wildcard with proper domain hostname
* cloudinit    - adjust cloudinit

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------
* hostname - the value for the hostname (default: localhost)
* domain   - the value for the domain (default: localdomain)

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - aws_config

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
