ecs_config
=========
Configure the ECS cluster and instance, includes the following roles:

* docker              - install and configure Docker and directories for ECS
* ecs_agent           - install and configure ECS agent

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - ecs_config

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
