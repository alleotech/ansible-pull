MySQL Server
=========
Install and configure MariaDB server

* get and configure MariaDB server 8.0
* install MariaDB server
* ensure proper permissions on data directory
* enable MariaDB server
* configure root user
* configure database for remote access

Tasks
-----

mysql_server

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

* db_pass    - database password stored in Ansible host_vars
* db_host    - database host stored in Ansible host_vars

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - mysql_server

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
