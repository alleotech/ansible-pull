nat
=========

Enable IPforwarding and configuring IPTables for NAT instance and private subnet, includes the following roles:

* iptables_config - install and flush IPTables
* nat_forwarding - enable IP Forwarding
* iptables_postrouting   - configure IPTables POSTROUTING and save IPTables configuration

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nat

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
