mysql_client
=========
Install and configure MySQL client

Tasks
-----

* mysql_client - install and configure MySQL client

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

* db_host
* db_user
* db_pass

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - mysql_client

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
