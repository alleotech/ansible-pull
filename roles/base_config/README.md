Role base_config
=========

Performs base configuration of the Linux system:

Tasks
----------
* selinux - checks if SELinux is in place and disables the default configuration
* swapfile - creates the swapfile on the system
* allow_root_ssh - allows root access over SSH

Requirements
------------

N/A

Role Variables
--------------

* swapfile_path - path to the swap
* swapfile_size - swap file size


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - base_config

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
