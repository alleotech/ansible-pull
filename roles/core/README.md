core
=========
Core instance configuration, includes the following roles:

* core_tools        - installing python-pip and Boto, installing and configuring AWS CLI
* core_config       - configure CloudMap script and Cron job, NFS exports, install NFS server, export NFS shared,  Hashbackup and Cron job


Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - core

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
