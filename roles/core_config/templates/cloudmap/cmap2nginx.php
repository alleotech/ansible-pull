#!/bin/env php
<?php

define("UPSTREAM_PATH", "/mnt/data/services/nginx-lb/etc/nginx/conf.d/upstreams");
define("VPC", "{{ ecs_namespace }}");

# Get a list of services in the CloudMap
$services = json_decode(`/usr/local/bin/aws servicediscovery list-services --query "Services[*].{Id: Id, Name: Name}"`, true);

foreach ($services as $service) {

    $id = $service['Id'];
    $name = $service['Name'];

    $instances = json_decode(`/usr/local/bin/aws servicediscovery list-instances --service-id $id --query "Instances[*].Attributes.{Ip: AWS_INSTANCE_IPV4, Port: AWS_INSTANCE_PORT, Status: AWS_INIT_HEALTH_STATUS, Cluster: ECS_CLUSTER_NAME}"`, true);

    $upstreams = [];
    foreach ($instances as $instance) {
        if (strpos($instance['Cluster'], VPC) === false) {
            continue;
        }
        if ($instance['Status'] !== 'HEALTHY') {
            continue;
        }
        $upstreams[$instance['Ip']] = $instance['Port'];
    }

    if (!empty($upstreams)) {
        $content = "upstream $name {\n"
            . "\tleast_conn;\n";
        foreach ($upstreams as $ip => $port) {
            $content .= "\tserver $ip:$port;\n";
        }
        $content .= "}";
        file_put_contents(UPSTREAM_PATH . "/$name.conf", $content);
    }

    // sleep for 3 seconds to cooldown AWS throttling
    sleep(3);
}

