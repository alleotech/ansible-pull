#!/bin/bash

#
# Daily cron job for the HashBackup with sensible defaults.
# More info: http://www.hashbackup.com/
# 
# Quick emergency help:
# 
# * List all backups      : {{ hashbackup_exe }} versions -c "{{ hashbackup_dir }}" -a
# * Mount all backups     : {{ hashbackup_exe }} mount -c "{{ hashbackup_dir }}" /mnt -f
# * List all files        : {{ hashbackup_exe }} ls -c "{{ hashbackup_dir }}" -l 
# * Compare disk to backup: {{ hashbackup_exe }} compare -c "{{ hashbackup_dir }}" -X /some/path
# * Restore files         : {{ hashbackup_exe }} get -c "{{ hashbackup_dir }}" /some/path 
# 

HASHBACKUP="{{ hashbackup_exe }}"
BACKUP_DIR="{{ hashbackup_dir }}"
CHECK_HB_UPDATES="{{ hashbackup_updates }}"
VERBOSE={{ hashbackup_verbose }}
DEDUP_MEMORY="{{ hashbackup_dedup }}"
MAX_FILE_SIZE="{{ hashbackup_max_file }}"
RETAIN_POLICY="{{ hashbackup_retain }}"
BACKUP_TARGETS="{{ hashbackup_list }}"
TAG="DailyBackup-$(date +'%F')"

# Check for the newer version fo HashBackup
if [ "$CHECK_HB_UPDATES" == "yes" ]
then
	echo "Checking for updates"
	$HASHBACKUP upgrade --force
	echo "--------------------"
fi

# Initialize the backup directory if it's not there
if [ ! -d "$BACKUP_DIR" ]
then
	echo "Initializing backup directory"
	mkdir -p "$(dirname $BACKUP_DIR)"
	$HASHBACKUP init -c "$BACKUP_DIR"
	echo "-----------------------------"
fi

# Do the backup now
echo "Backing up tag $TAG"
$HASHBACKUP backup -c "$BACKUP_DIR" -X -v$VERBOSE -t "$TAG" -m "$MAX_FILE_SIZE" -D "$DEDUP_MEMORY" $BACKUP_TARGETS
echo "-------------------------------------"

# Aply retention policy
echo "Applying retention policy"
if [ "$RETAIN_POLICY" == "-t all" ]
then
	echo "Retention policy is '$RETAIN_POLICY'. Skipping."
else
	$HASHBACKUP retain -c "$BACKUP_DIR" $RETAIN_POLICY
fi
echo "-------------------------"
