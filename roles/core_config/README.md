core_config
=========

Configure Core instance cloudmap, hashbackup and NFS

Tasks
----------

* cloudmap  - configures cloudmap script and cron job
* nfs - installs and configures NFS server and configures the exports
* hashbackup  - install Hashbackup and Hashbackup cron jobs


Requirements
------------

N/A

Role Variables (defaults/vars)
------------------------------

* ecs_namescpace - the namespace for the ECS cluster

* nfs_items
** rw
** async
** no_root_squash
** no_all_squash
** no_subtree_check

* hashbackup_exe
* hashbackup_dir
* hashbackup_list
* hashbackup_updates
* hashbackup_verbose
* hashbackup_dedup
* hashbackup_max_file
* hashbackup_retain


Dependencies
------------

templates/cloudmap/cmap2nginx.php
templates/hashbackup/hashbackup.sh
templates/hashbackup/hb


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - core_config

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
