ecs_agent
=========

Configure ECS cluster, directories and agent

Tasks
----------

<<<<<<< HEAD
* ecs_agent  -  configure and provision ECS agent
=======
* ecs_agent  -  configuration
>>>>>>> 9028891a623bf0362ca9d8ba09f3db6fd0e6c945

Requirements
------------

N/A

Role Variables (defaults/vars)
------------------------------

* ecs_env_file      - main ECS cluster config file
* ecs_cluster       - the cluster name
* ecs_config        - cluster instance configuration
* ecs_directories   - log and lib directories
* ecs_volumes       - ECS instance volumes configuration
* ecs_image         - latest ECS agent image

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - ecs_agent

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
