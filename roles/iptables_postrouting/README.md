iptables_postrouting
=========

Update IPTables configuration for the NAT and private subnet attachment

Tasks
-----

* iptables  - configure the IPTables postrouting


Requirements
------------

* IPTables installation (can be used from the iptables_config roles)


Role Variables (defaults/vars)
--------------

* subnet_private   - CIDR block of the private subnet to enable routing from


Dependencies
------------

* IPTables


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - iptables_postrouting


License
-------

BSD


Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
