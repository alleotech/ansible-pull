filesystem
=========
Ansible role to configure the file system on the Core and DB instances
* uses the condition to configure the file system for each of the Core and the DB instances

Tasks
----------

* filesystem     - installs yum packages and base tools
* fs             - formats and mounts the drive

Requirements
------------

N/A

Role Variables (Defaults)
--------------

* core_volumes - the main and the backup drives, size and mount points for the Core instance volumes
* db_volumes   - the main and the backup drives, size and mount points for the DB instance volumes

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - filesystem

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
