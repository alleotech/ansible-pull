iptables_prerouting
=========

Adjust the NAT IPTables configuration for the ECS


Tasks
----------

* iptables  - adjusts Adjust IPTables NAT for ECS [Prerouting], adjust IPTables NAT for ECS [Output], save IPTables configuration


Requirements
------------

* IPTables must be installed and preconfigured (can be used from the iptables_config role)


Role Variables (defaults/vars)
------------------------------

N/A

Dependencies
------------

* IPTables package


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - iptables_prerouting


License
-------

BSD


Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
