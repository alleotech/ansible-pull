iptables_config
=========

Install and run IPTables:

Tasks
----------
* iptables - installs IPTables, runs IPTables when not in Docker

Requirements
------------

N/A

Role Variables (defaults/vars)
------------------------------

* iptables_packages - [iptables, iptables-services]

Handlers
--------

Flush and save IPTables configuration


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - iptables_config

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
