composer
=========

Install and configure composer globally

* download and install composer
* rename composer.phar to composer
* make composer executable


Tasks
-----

composer

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

* remi_repo           - Path to Remi repo
* remi_version        - PHP version from Remi repo
* php_packages        - List of PHP packages to install
* service_directories - path to PHP lib

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - php

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
