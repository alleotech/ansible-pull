nat_forwarding
=========
Enable IPforwarding

Tasks
-----

* forwarding  - configure sysctl to enable NAT IP forwarding

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nat_forwarding

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
