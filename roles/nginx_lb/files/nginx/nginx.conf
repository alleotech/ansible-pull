user nobody;

error_log   /var/log/nginx/error.log warn;
pid         /var/run/nginx.pid;

load_module "modules/ngx_http_geoip_module.so";

worker_processes  auto;

events {
    worker_connections 1024;
    use epoll;
    multi_accept on;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    server_tokens off;

    geoip_country /usr/share/GeoIP/GeoIP.dat;

    # Connection rage limits
    limit_conn_status 429;
    limit_req_status 429;

    map $http_user_agent $isbot_ua {
        default 0;
        ~*(bot|spider|crawl|feed) 1;
    }
    map $isbot_ua $limit_bot {
        0       "";
        1       $binary_remote_addr;
    }
    limit_req_zone $limit_bot zone=bots:10m rate=5r/m;
    limit_req zone=bots burst=15 nodelay;

    # Limit number of connections per IP
#    limit_conn_zone $binary_remote_addr zone=addr:10m;
#    limit_conn addr 30;

    # Limit number of requests/s
#    limit_req_zone $binary_remote_addr zone=req:10m rate=30r/s;
#    limit_req zone=req burst=50;
    #limit_req zone=req burst=15 nodelay;

    server_names_hash_bucket_size 64;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    log_format timed_combined   '$remote_addr - $remote_user [$time_local] '
                      '"$request_method $scheme://$host$request_uri $server_protocol" $status $body_bytes_sent '
                      '"$http_referer" "$http_user_agent" '
                      '$request_time $upstream_response_time';

    access_log  /var/log/nginx/access.log main;

    sendfile        on;
    tcp_nopush      on;
    tcp_nodelay     on;

    reset_timedout_connection on;
    client_body_timeout 60;
    client_max_body_size 20m;

    keepalive_timeout  3;
    keepalive_requests 100;

    send_timeout 60;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # FastCGI setup

    # Hopefully solving problems with occasional "502 Bad Gateway" errors
    # Thanks to: http://jvdc.me/fix-502-bad-gateway-error-on-nginx-server-after-upgrading-php/
    # http://stackoverflow.com/questions/23844761/upstream-sent-too-big-header-while-reading-response-header-from-upstream
    fastcgi_buffers 16 16k;
    fastcgi_buffer_size 32k;
    fastcgi_connect_timeout 300;
    fastcgi_send_timeout 300;
    fastcgi_read_timeout 300;
    fastcgi_index index.php;

    # FastCGI cache setup
    fastcgi_cache_path /var/cache/nginx/fastcgi levels=1:2 keys_zone=FASTCGI:1024m;
    fastcgi_cache_key "$scheme$request_method$host$request_uri";
    fastcgi_cache_use_stale error timeout invalid_header updating http_500 http_503;
    fastcgi_cache_lock on;
    fastcgi_cache_revalidate off;

    fastcgi_cache_min_uses 1; # from 2

    # FastCGI cache usage (global)
    fastcgi_cache FASTCGI;
    fastcgi_cache_valid 10s;
    add_header X-Cache $upstream_cache_status;

    # Set correct character set
    charset utf-8;
    charset_types
        application/javascript
        application/json
        application/x-javascript
        application/xml
        application/xml+rss
        text/css
        text/javascript
        text/plain
        text/xml
    ;
    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css text/xml application/javascript application/json application/x-javascript application/xml application/xml+rss text/javascript;

    #SSL configuration
	ssl_session_cache shared:SSL:50m;
	ssl_session_timeout 1d;
	ssl_session_tickets on;

	ssl_protocols TLSv1.2 TLSv1.3;
	ssl_ciphers TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-AES-128-GCM-SHA256:EECDH+CHACHA20:EECDH+AESGCM:EECDH+AES;

	ssl_prefer_server_ciphers on;
	ssl_dhparam /etc/nginx/dhparam.pem;
	ssl_stapling on;
	ssl_stapling_verify on;
    index   index.php index.html index.htm;

    include /etc/nginx/conf.d/*/*.conf;
}

