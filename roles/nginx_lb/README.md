nginx_lb
=========

Nginx and Certbot configuration for the Load Balancer on the Core instance:

* ensure service directories in place
* ensure lib directory permissions
* ensure nginx module symlink in place
* generate HDParam
* provision Nginx configuration
* clone Certbot
* link certbot to global path
* link letsencrypt to NFS
* install Certbot cron job


Tasks
-----

* nginx -  configure NGINX load balancing for the Core instance


Requirements
------------

N/A

Role Variables (defaults/vars)
--------------


* ssl_email                 - email address used for ceftbot certification
* nginx_service_directories - ensures that these service directories are in place
* nginx_config              - configuration files provisioned to the instance

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nginx_lb

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
