nvme
=========
Mapping the ebs devices and installing the udev rules:

* nvme - install and configure ebs mapping and udev rules

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

* files/999-aws-ebs-nvme.rules
* files/ebs-nvme-mapping.sh

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nvme

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
