#!/bin/bash

HASH_FILE=/tmp/nginx_conf.md5
OLD_HASH=''

if [ -f "$HASH_FILE" ];
then
    OLD_HASH=`cat $HASH_FILE`
fi

NEW_HASH=`find /etc/nginx/ /etc/letsencrypt/ -type f -exec md5sum {} \; | sort -k 2 | md5sum | cut -d ' ' -f 1`

if [ "$OLD_HASH" != "$NEW_HASH" ];
then
    /usr/sbin/nginx -t && /usr/sbin/service nginx reload
    echo "$NEW_HASH" > $HASH_FILE
fi
