nginx_nat
=========

NGINX NAT configuration

* configure Nginx repo
* install Nginx
* check Nginx configuration is not on NFS
* configure Nginx
* remove default Nginx configuration
* provision Nginx configuration
* create /var/www/ directory
* link NFS configurations
* provision Nginx reload script
* set nginx reload cron job
* enable Nginx 

Tasks
-----

* nginx

Requirements
------------

Nginx

Role Variables (defaults/vars)
--------------

* nginx_packages       - the list of the additional Nginx modules to install
* remove_default_dirs  - the list of the default directories to remove
* nginx_nfs_config     - the Nginx direcotories linked to the NFS configuration

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - nginx_nat

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
