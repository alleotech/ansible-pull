Role system_base
=========

System base role which includes the following roles to configure the filesystem, basic tools and packages. The role is used on all the instances. Includes the following roles:

* base_config     - Performs base configuration of the Linux system, disables SELinux, enables root SSH access, creates swapfile
* base_tools      - installs and configure the base packages and tools
* base_filesystem - create the file system, format and configure the targets

Requirements
------------

N/A

Role Variables (defaults/vars)
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - system_base

License
-------

BSD

Author Information
------------------

AlleoTech Ltd <admin@alleo.tech>
